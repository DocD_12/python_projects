if __name__ == "__main__":
    fin = open('input.txt')
    A, B = map(int, fin.read().split())
    fout = open('output.txt', 'w')
    fout.write(str(A + B))