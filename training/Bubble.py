import random

def quicksort(nums):
    if len(nums) <= 1:
        return nums
    else:
        q = random.choice(nums)
        s_nums = []
        m_nums = []
        e_nums = []
        for n in nums:
            if n < q:
                s_nums.append(n)
            elif n > q:
                m_nums.append(n)
            else:
                e_nums.append(n)
        return quicksort(s_nums) + e_nums + quicksort(m_nums)

if __name__ == "__main__":
    nums = [4, 1, 6, 3, 2, 7, 8]
    nums2 = nums
    n = 1
    while n < len(nums):
       for i in range(len(nums) - n):
           if nums[i] > nums[i + 1]:
               nums[i], nums[i + 1] = nums[i + 1], nums[i]
       n += 1

    print(nums)
    print(quicksort(nums2))