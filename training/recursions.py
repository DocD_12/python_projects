def factorial(n):
    if n == 0:
        return 1
    else:
        return n * factorial(n - 1)

def fib(n):
    if n == 0 or n == 1:
        return n
    else:
        return fib(n - 2) + fib(n - 1)

if __name__ == "__main__":
    print(factorial(5))
    print(fib(5))