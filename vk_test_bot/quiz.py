__author__ = 'Truck'

import settings
import googletables
import vkhelp
import bot
import vk_api
from vk_api.longpoll import VkLongPoll, VkEventType
from vk_api.utils import get_random_id
import time

def lets_quiz(vk, longpoll):
    print("Quiz started")

    iq = 1
    iqmax = settings.iqmax

    vkhelp.sendmes(vk, settings.start_message)
    print("start message done")

    gglsht = googletables.open()
    print("google sheet opened")

    user_list = googletables.take_list(gglsht)
    print(type(user_list))

    while iqmax >= iq:

        question = googletables.take_quest(gglsht, iq)
        answer = googletables.take_answer(gglsht, iq)
        tip = googletables.take_tip(gglsht, iq)
        b = ''.join(answer.split())
        answer_list = b.lower().split('/')
        tip_flag = 1

        print(question, answer, tip, answer_list, sep='\n')

        vkhelp.sendmes(vk, "Вопрос №"+ str(iq) + "\n\n" + question)


        start_time = time.time()
        for event in longpoll.listen():
            if (time.time() - start_time > 20) and tip_flag and tip:
                vkhelp.sendmes(vk, "Прошло больше 20секунд, держите легкий намёк: \n\n" + tip)
                tip_flag = 0


            if (time.time() - start_time > 30):
                vkhelp.sendmes(vk, "Время вышло, правильный ответ был: \n\n" + answer + "\n Никто не получает балл, едем дальше.")
                iq += 1
                break


            if event.type == VkEventType.MESSAGE_NEW and event.text and event.to_me:
                b = event.text.split()
                b = ''.join(b)
                if b.lower() in answer_list:
                    userisright = event.user_id
                    user_list = googletables.add_score(gglsht, userisright, iq, user_list)
                    user_get = vk.users.get(user_ids = (userisright))[0]
                    mes = "Правильный ответ: " + answer + "\nОтвет " + vkhelp.take_fname(user_get) + " оказался категорически верным."
                    vkhelp.sendmes(vk, mes)
                    iq += 1
                    break

    vkhelp.sendmes(vk, "Вот и закончилась наша викторина, смотрим результаты.")
    list = googletables.get_scores(gglsht)
    vkhelp.showscores(vk, list)