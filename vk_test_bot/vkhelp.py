__author__ = 'Truck'

import bot
import settings
from vk_api.utils import get_random_id

def take_fname(user_get):
    first_name=user_get['first_name']
    last_name=user_get['last_name']
    return first_name+" "+last_name

def sendmes(vk, inmessage):
    vk.messages.send(
        chat_id=settings.test_chat,
        random_id=get_random_id(),
        message=inmessage
    )

def showscores(vk, list):
    mes = " Табличка результатов:\n"
    for i in list:
        mes += take_fname(list[i][0]) + ": " + list[i][1] + "правильных ответов\n"

    mes += "Всем спасибо за участие!"

    sendmes(vk, mes)