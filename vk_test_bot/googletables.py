import gspread
import settings


# Open a sheet from a spreadsheet in one go
def open():
    gc = gspread.service_account(filename=settings.json_file)
    sh = gc.open_by_url(settings.table_url)
    sheet = sh.sheet1
    return sheet

def test(worksheet):
    val = worksheet.get('C3').first()
    print(val)
    print('Ответ')
    print(worksheet.cell(3,6).value)

def take_quest(worksheet, question_number):
    return worksheet.cell(settings.frst_row+question_number-1,settings.quest_col).value

def take_tip(worksheet, question_number):
    return worksheet.cell(settings.frst_row+question_number-1,settings.tip_col).value

def take_answer(worksheet, question_number):
    return worksheet.cell(settings.frst_row+question_number-1,settings.ans_col).value

def check_number(worksheet, question_number):
    if (worksheet.cell(settings.frst_row+question_number-1,settings.num_col).value == question_number):
        return True
    else:
        return False

def take_list(worksheet):
    valueofcell = worksheet.cell(settings.user_row,settings.user_zero_col).value
    if valueofcell == '':
        allnum = 0
    else:
        allnum = int(valueofcell)
    list = []
    if allnum == 0:
        return list
    else:
        for i in range(allnum):
            list[i] = worksheet.cell(settings.user_row,settings.user_zero_col+i).value
        return list

def add_score(worksheet, usid, question_number, list):
    valueofcell = worksheet.cell(settings.user_row,settings.user_zero_col).value
    if valueofcell == '':
        allnum = 0
    else:
        allnum = int(valueofcell)
    if usid in list:
        worksheet.update_cell(settings.user_row + question_number, settings.user_zero_col + list.index(usid), 1)
    else:
        list.append(usid)
        allnum += 1
        worksheet.update_cell(settings.user_row, settings.user_zero_col, allnum)
        worksheet.update_cell(settings.user_row + question_number, settings.user_zero_col + list.index(usid), 1)
    return list

def get_score(worksheet, usid):
    if usid in list:
        return worksheet.cell(settings.user_row - 1, settings.user_zero_col + list.index(usid)).value
    else:
        return 0

def get_scores(worksheet):
    valueofcell = worksheet.cell(settings.user_row,settings.user_zero_col).value
    if valueofcell == '':
        allnum = 0
    else:
        allnum = int(valueofcell)
    list = []
    for i in range(allnum):
        list.append(worksheet.cell(settings.user_row, settings.user_zero_col + i).value)
        list[i].append(worksheet.cell(settings.user_row-1, settings.user_zero_col + i).value)
    return list

if __name__ == '__main__':
    test(open())