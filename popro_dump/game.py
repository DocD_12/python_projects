import player
import random
import cards
#import app

class Game:
	numpl = 0
	numbots = 0
	players = list()
	dealer = 0
	bank = 0
	bet = 0
	blind = 0
	deck = list()
	turn = 0
	roll = 0
	
	def __init__(self, npl = 2, nbts = 1, blind = 2, roll = 100):
		#check players numeric
		if npl > 5:
			npl = 5
		if npl < 1:
			npl = 1
		self.numpl = npl
		#check bots numeric
		if nbts > npl:
			nbts = npl
		if nbts < 0:
			nbts = 0
		self.numbots = nbts
		#player initializtion
		id = 1
		for i in range(npl):			
			botcounter = 0
			if (botcounter < nbts):
				self.players.append(player.Player(roll, 1, id))
				botcounter += 1
			else:
				self.players.append(player.Player(roll, 0, id))
			id += 1
		#other staff
		self.dealer = random.randint (1, self.numpl)
		self.roll = roll
		self.bet = 10
		self.blind = blind
		self.deck = cards.Cards()
		self.turn = 0
		self.bank = 0

	def getnumpl(self):
		return self.numpl

	def call(self):
		return 0