fvalues = ("Zero","One","Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace")
values = ('Z','!','2','3','4','5','6','7','8','9','X','J','Q','K','A')
suits = ("hearts ", "diamonds", "clubs", "spades")
psuits = ("♥", "♦", "♣", "♠")

def getfullvalbynum(n):
    return fvalues[n]

class Card:
    def __init__(self, value, suit):
        if (value < 2):
            value = 2
        if (value > 14):
            value = 14
        self.value = value

        if (suit < 0):
            suit = 0
        if (suit > 3):
            suit = 3
        self.suit = suit



    def showcard(self):
        str = " -------- "
        str = str + '\n' + "| {:<2}     |".format(values[self.value])
        str = str + '\n' + "|        |"
        str = str + '\n' + "|{:^8}|".format(suits[self.suit])
        str = str + '\n' + "|        |"
        str = str + '\n' + "|     {:>2} |".format(values[self.value])
        str = str + '\n' + " -------- "
        return str

    def showcard_fancy(self):
        str = "|{1:}{0:.1}|".format(psuits[self.suit], values[self.value][-1])
        return str

    def showcard_sh(self):
        str = "{1:}{0:.1}".format(suits[self.suit], values[self.value][-1])
        return str

    def showcard_rt(self):
        str = "|{1:}{0:.1}|".format(suits[self.suit], values[self.value][-1])
        return str

    def showback_rt(self):
        return '|**|'

    def getvalue(self):
        return self.value

    def getsuit(self):
        return self.suit

    def getpvalue(self):
        return values[self.value]

    def getpsuit(self):
        return psuits[self.suit]
