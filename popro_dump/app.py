import tkinter as tk
import game
import card

class Application(tk.Frame):

	def action(self, args):
		if (args == 1):
			print('Fold')
			self.next
		elif (args == 2):
			print('Call')
			game.call()
		elif (args == 3):
			print('Rise')
			nextturn()

	def next(self):
		self.game.nextturn()

	def __init__(self, master = None):
		tk.Frame.__init__(self, master, width = 200)
		self.game = game.Game(1,0)		
		self.pack()

		self.createPlayersFrame()
		self.createBankFrame()
		self.createCardsFrame()
		self.createControlFrame()

		# self.createButtons()
		# self.createWidgetLabels()
		# self.createWidgetOther()
		
		print('Application initialized')		

	def createPlayersFrame(self):
		self.playersFrame = tk.Frame(self)
		self.playersFrame.pack(side = 'left')
		tk.Label(self.playersFrame, text = "Player's Labels")
		print("playersFrame created")

	def createBankFrame(self):
		print('bankFrame created')

	def createCardsFrame(self):
		print('cardsFrame created')

	def createControlFrame(self):
		print('controlFrame created')

	def createButtons(self):
		self.buttonsFrame = tk.Frame(self)
		self.buttonsFrame.pack(side = 'right')

		self.quitButton = tk.Button(self.buttonsFrame, text='Quit', command=self.quit)
		#self.quitButton.pack(side = 'right')
		self.riseButton = tk.Button(self.buttonsFrame, text='Rise', command=lambda:self.action(3))
		#self.riseButton.pack(side = 'right')        
		self.callButton = tk.Button(self.buttonsFrame, text='Call', command=lambda:self.action(2))
		#self.callButton.pack(side = 'right')        
		self.foldButton = tk.Button(self.buttonsFrame, text='Fold', command=lambda:self.action(1))
		#self.foldButton.pack(side = 'right')

		# self.oneCard = tk.PhotoImage(file = self.loadCardpath(card.Card(12,3)))
		# self.oneCard = self.oneCard.subsample(6, 6)
		self.ocLabel = tk.Label(text = card.Card(12,3).showcard_rt())
		self.ocLabel.pack(side = 'bottom')	

		# self.tCard = tk.PhotoImage(file = self.loadCardpath(card.Card(15,3)))
		# self.tCard = self.tCard.subsample(6, 6)
		self.tcLabel = tk.Label(text = card.Card(15,3).showcard_rt())
		self.tcLabel.pack(side = 'bottom')	
		print('Buttons initialized')

	def createWidgetLabels(self):
		self.betMyLabel = tk.Label(self, text = "Cur BET: ")
		self.betMyLabel.pack()
		self.helpLabel = tk.Label(self, text = ":) ")
		self.helpLabel.pack()
		self.nameLabel = list()
		self.betLabel = list()
		print('Labels initialized')

	def createWidgetOther(self):
		self.riseSpin = tk.Spinbox(self)
		self.riseSpin.pack()
		self.bankText = tk.Label(self, text = "BANK-ROLL: ", width = 20)
		self.bankText.pack()
		self.deskCard = list()
		for i in range(5):
			dCard = tk.Label(image = (tk.PhotoImage(file = 'PNG/red_back.png').subsample(6, 6)))
			dCard = tk.Label(text = card.Card(12,3).showback_rt())
			self.deskCard.append(dCard)
			self.deskCard[-1].pack(side ='left')
		print('Others initialized')

	def createPlayers(self, num):
		for i in range(num):
			self.nameLabel.append(tk.Label(playersFrame, text = "Player %d" % i))
			self.betLabel.append(tk.Label(playersFrame, text = "bet %d" % i))

	def loadCardpath(self, card):
		values = ('2','3','4','5','6','7','8','9','10','J','Q','K','A')
		suits = ("H", "D", "C", "S")
		return 'PNG/' + card.getpvalue() + card.getpsuit() + '.png'

	def loadBackpath(self):
		#backCard = tk.PhotoImage(file = 'PNG/purple_back.png')
		bcl = 'PNG/AS.png'
		return bcl

	def clearTable(self):
		return 0