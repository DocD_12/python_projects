import card
import random

class Cards:
    deck = list()

    def __init__(self):
        count = 0
        for va in range(2,15):
            for su in range(4):
                self.deck.append(card.Card(va, su))

    def showdeck(self):
        count = 0
        for ca in self.deck:
            ca.showcard_sh()

    def prepdeck(self):
        random.shuffle(self.deck)

    def getcard(self):
        i = random.randint(0,len(self.deck) - 1)
        el = self.deck[i]
        del(self.deck[i])
        return el

class Combination:

    def __init__(self):
        self.comb = []
        self.weight = [0, 0, 0 ,0 ,0, 0]

    def addcard(self, card):
        i = [card.getvalue(), card.getsuit()]
        self.comb.append(i)

    def showcomb(self):
        print (self.comb)

    def showcomb_fancy(self):
        str = ''
        for i in self.comb:
            card1 = card.Card(i[0], i[1])
            str = str + ' ' + card1.showcard_fancy()
        return str

    def sortcomb(self):
        print(self.comb)
        self.comb.sort(reverse=True)
        print(self.comb)

    def four_check(self, istart):
        str_count = 0
        if istart > 4: istart = 4
        start = self.comb[istart][0]
        for i in self.comb:
            if i[0] == start:
                str_count += 1
        return str_count

    def straight_check(self, istart):
        str_count = 0
        flsh_count = 0
        start_suit = self.comb[istart][1]
        start = self.comb[istart][0]
        for i in self.comb:
            if i[0] == start - 1:
                str_count += 1
                start = i[0]
                if i[1] == start_suit: flsh_count += 1
        if self.comb[-1][0] == 0 and self.comb[0][0] == 12:
            str_count += 1
            if self.comb[0][1] == start_suit: flsh_count += 1
        if str_count >= 4:
            if flsh_count >= 4: return 9
            return 5
        else: return 0

    def checkcomb(self):
        flag = 0
        for i in range(len(self.comb) - 1):
            start = i
            if self.comb[i][0] == self.comb[i+1][0] + 1:
                if flag < self.straight_check(start):
                    flag = self.straight_check(start)
                    if flag == 5:
                        self.weight[0] = 5
                        self.weight[1] = self.comb[start][0]
                    if flag == 9:
                        self.weight[0] = 9
                        self.weight[1] = self.comb[start][0]
            if self.comb[i][0] == self.comb[i+1][0]:
                if flag < self.four_check(start):
                    flag = self.four_check(start)
                    if flag == 4:
                        self.weight[0] = 8
                        self.weight[1] = self.comb[start][0]
                    if flag == 3:
                        self.weight[0] = 4
                        self.weight[1] = self.comb[start][0]
                        if self.four_check(start + 3) == 2:
                            self.weight[0] = 7
                            self.weight[1] = self.comb[start][0]
                            self.weight[2] = self.comb[start + 3][0]
                        elif self.four_check(0) == 2:
                            self.weight[0] = 7
                            self.weight[1] = self.comb[start][0]
                            self.weight[2] = self.comb[0][0]
                    if flag == 2:
                        self.weight[0] = 2
                        self.weight[1] = self.comb[start][0]
                        if self.four_check(start + 2) == 3:
                            self.weight[0] = 7
                            self.weight[1] = self.comb[start + 2][0]
                            self.weight[2] = self.comb[start][0]
                        if (self.four_check(start + 2) == 2 or
                            self.four_check(start + 3) == 2):
                            self.weight[0] = 3
                            self.weight[1] = self.comb[start][0]
                            self.weight[2] = self.comb[start + 3][0]
        if flag == 0:
            self.weight[0] = 1
            self.weight[1] = self.comb[0][0]
        added = 5
        cnt = 5
        while added > 0 and cnt > 0 and not self.weight[0] in (5, 6, 7, 9):
            if self.comb[5 - cnt][0] != self.weight[1]:
                self.weight[7 - added] = self.comb[5 - cnt][0]
                added -= 1
            cnt -= 1



    def printcomb(self):
        if self.weight[0] == 9:
            str = "A " + card.getfullvalbynum(self.weight[1]) +  "-high Straight Flush"
            return str
        if self.weight[0] == 8:
            str = "Four of a kind, " + card.getfullvalbynum(self.weight[1]) +  "s"
            return str
        if self.weight[0] == 7:
            str = "A full house, " + card.getfullvalbynum(self.weight[1]) +  "s over " + card.getfullvalbynum(self.weight[2]) + "s"
            return str
        if self.weight[0] == 6:
            str = "A " + card.getfullvalbynum(self.weight[1]) +  "-high Flush"
            return str
        if self.weight[0] == 5:
            str = "A " + card.getfullvalbynum(self.weight[1]) +  "-high Straight"
            return str
        if self.weight[0] == 4:
            str = "Three of a kind, " + card.getfullvalbynum(self.weight[1]) +  "s"
            return str
        if self.weight[0] == 3:
            str = "Two pair, " + card.getfullvalbynum(self.weight[1]) +  "s and " + card.getfullvalbynum(self.weight[2]) + "s"
            return str
        if self.weight[0] == 2:
            str = "One pair, " + card.getfullvalbynum(self.weight[1]) +  "s"
            return str
        if self.weight[0] == 1:
            str = "High card, " + card.getfullvalbynum(self.weight[1])
            return str
        if self.weight[0] == 0:
            str = "ERROR"
            return str

    def printkicks(self):
        for i in range(2, 6):
            if self.weight[i] != 0:
                print('Kicker ' + str(i-1) + ' is ' + str(self.weight[i]), end=" | ")