import cards

if __name__ == "__main__":
    cc = cards.Cards()
    cc.prepdeck()
    combo = cards.Combination()
    for i in range(2):
        card_1 = cc.getcard()
        combo.addcard(card_1)
        card_1.showcard_fancy()
    cc.getcard()
    print("  ", end='')
    for i in range(3):
        card = cc.getcard()
        combo.addcard(card)
        card.showcard_fancy()

    # card123 = [[15,0], [13,0], [12,0] ,[5,0] ,[4,0] ,[3,0] ,[2,0]]
    # card123 = [[11, 3], [5, 2], [9, 0], [10, 1], [3, 1]]
    # card123 = [[11, 3], [11, 2], [9, 0], [10, 1], [3, 1]]
    # card123 = [[7, 2], [6, 0], [6, 2], [7, 0], [7, 1]]
    # combo = cards.Combination()
    # for i in card123:
    #     card1 = card.Card(i[0], i[1])
    #     card1.showcard_fancy()
    #     combo.addcard(card1)

    print()
    combo.sortcomb()
    combo.checkcomb()
    combo.printcomb()
    combo.showcomb_fancy()
    combo.printkicks()
