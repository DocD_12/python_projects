import telebot
import cards

def main(args):
	return 0

if __name__ == '__main__':
	# Создаем экземпляр бота
	bot = telebot.TeleBot()

	# Функция, обрабатывающая команду /start
	@bot.message_handler(commands=["start"])
	def start(m, res=False):
		bot.send_message(m.chat.id, 'Раскидаем покерок')

		cc = cards.Cards()
		cc.prepdeck()
		combo = cards.Combination()
		for i in range(2):
			card_1 = cc.getcard()
			combo.addcard(card_1)
			card_1.showcard_fancy()
		bot.send_message(m.chat.id, combo.showcomb_fancy())
		print(combo.showcomb_fancy())
		cc.getcard()
		print("  ", end='')
		for i in range(3):
			card = cc.getcard()
			combo.addcard(card)
			card.showcard_fancy()

	# Получение сообщений от юзера
	@bot.message_handler(content_types=["text"])
	def handle_text(message):
		bot.send_message(message.chat.id, message.text + "\nСерьезно?")

	# Запускаем бота
	bot.polling(none_stop=True, interval=0)
