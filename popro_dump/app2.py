import tkinter as tk
import game

class Application(tk.Frame):

	def action(self, args):
		if (args == 1):
			print('Button1')
			self.next
		elif (args == 2):
			print('Button2')
			self.next
		elif (args == 3):
			print('Button3')
			nextturn()

	def next(self):
		self.game.nextturn()

	def __init__(self, master = None):
		tk.Frame.__init__(self, master, width = 200)
		self.game = game.Game(1,0)
		self.grid()
		self.createWidgetButtons()
		self.createWidgetLabels()
		self.createWidgetOther()
		print('Application initialized')

	def createWidgetButtons(self):
		self.quitButton = tk.Button(self, text='Quit', command=self.quit)
		self.quitButton.grid(column = 6, row = 1)
		self.riseButton = tk.Button(self, text='Rise', command=lambda:self.action(3))
		self.riseButton.grid(column = 6, row = 4)        
		self.callButton = tk.Button(self, text='Call', command=lambda:self.action(2))
		self.callButton.grid(column = 6, row = 5)        
		self.foldButton = tk.Button(self, text='Fold', command=lambda:self.action(1))
		self.foldButton.grid(column = 6, row = 6)
		self.oneCard = tk.PhotoImage(file = 'PNG/2C.png')
		self.oneCard = self.oneCard.subsample(5, 5)
		self.ocLabel = tk.Label(image = self.oneCard)
		self.ocLabel.grid()		
		print('Buttons initialized')

	def createWidgetLabels(self):
		self.betMyLabel = tk.Label(self, text = "Cur BET: ")
		self.betMyLabel.grid(row =5, column = 5)
		self.helpLabel = tk.Label(self, text = ":) ")
		self.helpLabel.grid(row =1, column = 5)
		self.nameLabel = list()
		self.betLabel = list()
		print('Labels initialized')

	def createWidgetOther(self):
		self.riseSpin = tk.Spinbox(self)
		self.riseSpin.grid(row = 4, column = 5)
		self.bankText = tk.Label(self, text = "BANK-ROLL: ", width = 20)
		self.bankText.grid(row = 6, column = 5)
		self.fstCard = tk.Label(self, text = "|**|")
		self.scnCard = tk.Label(self, text = "|**|")
		self.fstCard.grid(row = 5, column = 1)
		self.scnCard.grid(row = 5, column = 2)
		self.deskCard = list()
		for i in range(5):
			self.deskCard.append(tk.Label(image = (tk.PhotoImage(file = 'PNG/2C.png').subsample(6, 6))))
			self.deskCard[i].grid(column = i, row =5)
		print('Others initialized')

	def createPlayers(self, num):
		for i in range(num):
			self.nameLabel.append(tk.Label(self, text = "Player %d" % i))
			self.betLabel.append(tk.Label(self, text = "bet %d" % i))
			self.nameLabel[i].grid(column = i, row = 1)
			self.betLabel[i].grid(column = i, row = 2)
		print('Players created')

	def loadCard(num, card):
		self.oneCard = tk.PhotoImage(file = 'PNG/2C.png')
		self.oneCard = self.oneCard.subsample(5, 5)
		print('Card loaded')

	def clearTable(self):
		return 0