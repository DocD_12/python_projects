import cards

deck = cards.Cards()
comb = cards.Combination()
cards = list()
n = int(input("Enter a number of turn: "))
if n >= 0:
    print('Hand')
    crd = deck.getcard()
    crd.showcard()
    comb.addcard(crd)
    crd = deck.getcard()
    crd.showcard()
    comb.addcard(crd)

if n >= 1:
    print('Flop')
    crd = deck.getcard()
    crd.showcard_sh()
    comb.addcard(crd)
    crd = deck.getcard()
    crd.showcard_sh()
    comb.addcard(crd)
    crd = deck.getcard()
    crd.showcard_sh()
    comb.addcard(crd)

if n >= 2:
    print('Turn')
    crd = deck.getcard()
    crd.showcard_sh()
    comb.addcard(crd)

if n >= 3:
    print('River')
    crd = deck.getcard()
    crd.showcard_sh()
    comb.addcard(crd)

comb.showcomb()
comb.checkcomb()

print ('The End')