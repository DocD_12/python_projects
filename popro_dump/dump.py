from classes import game_class
game = game_class.Game()


def show_log():
    for x, line in enumerate(game.log):
        print(x, line)
    game.log = []

while True:
    game.hand_init()

    game.process_hand()

    game.hand_result()

    if len(game.with_bank_plrs()) == 1:
        break

    game.hand_number += 1

show_log()  # Debug

winner, = game.plrs

print(winner.name, winner.bank)

# Часть 2

from .config_class import CardConfig as cfg


class Card(cfg):
    """Класс одной карты"""
    def __init__(self, sui, val):
        super(Card, self).__init__()
        self.sui = sui
        self.val = val + 2
        self.name = cfg.suits[self.sui] + cfg.vals[self.val - 2]

if __name__ == '__main__':
    main()

# Часть 3

from .config_class import ComboConfig as cfg
import copy


class Combo(cfg):
    """docstring for Combo"""
    def test_cards(self):
        normal_pool = sorted(self.pool, key=lambda card: card.val, reverse=True)

        pool_vals = [card.val for card in normal_pool]
        pool_suis = [card.sui for card in normal_pool]

        if 14 in pool_vals:
            ace_pool = copy.deepcopy(self.pool)
            for card in ace_pool:
                if card.val == 14:
                    card.val = 1
            ace_pool = sorted(ace_pool, key=lambda card: card.val, reverse=True)

        result = {}

        def put_result():
            self.cards = result['cards']

            cards_names = ' '.join([card.name for card in result['cards']])
            self.text = result['text'] + cards_names

            pre_power = ''.join([str(hex(card.val))[2:] for card in self.cards])

            if len(pre_power) == 2:
                pre_power += '000'

            power_0x = str(result['combo_index']) + pre_power

            self.power = int(power_0x, 16)

        def high_card():
            if self.power > 0:
                return None
            result['cards'] = normal_pool[:5]
            result['text'] = 'Старшая карта '
            result['combo_index'] = 0

            put_result()

        def one_pair():
            if self.power > 1:
                return None
            res = [card for card in normal_pool if pool_vals.count(card.val) == 2]

            if len(res) == 2:
                cards = [card for card in normal_pool if card.val != res[0].val]

                result['cards'] = res + cards[:3]
                result['text'] = 'Одна пара '
                result['combo_index'] = 1

                put_result()

        def two_pairs():
            if self.power > 2 or len(normal_pool) < 5:
                return None
            res = [card for card in normal_pool if pool_vals.count(card.val) == 2][:4]

            if len(res) == 4:
                cards = [card for card in normal_pool if card.val != res[0].val and card.val != res[2].val]

                result['cards'] = res + [cards[0]]
                result['text'] = 'Две пары '
                result['combo_index'] = 2

                put_result()

        def three():
            if self.power > 3 or len(normal_pool) < 5:
                return None
            res = [card for card in normal_pool if pool_vals.count(card.val) == 3]

            if len(res) == 3:
                cards = [card for card in normal_pool if card.val != res[0].val]

                result['cards'] = res + cards[:2]
                result['text'] = 'Тройка '
                result['combo_index'] = 3

                put_result()

        def straight():
            if self.power > 4 or len(normal_pool) < 5:
                return None

            def check_str(cards):
                straight_test = True
                x = 0
                while x < 4:
                    straight_test = straight_test and cards[x].val - 1 == cards[x+1].val
                    x += 1
                return {'straight_test': straight_test, 'cards': cards}

            res_list = []

            if 14 in pool_vals:
                x = 0
                while True:
                    pre_res = check_str(ace_pool[-5-x:][:5])
                    res_list.append(pre_res)
                    x += 1
                    if x == len(ace_pool) - 4:
                        break

            x = 0
            while True:
                pre_res = check_str(normal_pool[-5-x:][:5])
                res_list.append(pre_res)
                x += 1
                if x == len(normal_pool) - 4:
                    break

            for res in res_list:
                if res['straight_test']:

                    result['cards'] = res['cards']
                    result['text'] = 'Стрит '
                    result['combo_index'] = 4

                    put_result()

        def flush():
            if self.power > 5 or len(normal_pool) < 5:
                return None
            res = [card for card in normal_pool if pool_suis.count(card.sui) > 4]

            if res:
                result['cards'] = res
                result['text'] = 'Флеш '
                result['combo_index'] = 5

                put_result()

        def full_house():
            if self.power > 6 or len(normal_pool) < 5:
                return None
            three_res = [card for card in normal_pool if pool_vals.count(card.val) > 2]
            if three_res:
                two_res = [card for card in normal_pool if pool_vals.count(card.val) > 1 and card.val != three_res[0].val]
                if two_res:
                    result['cards'] = three_res[:3] + two_res[:2]
                    result['text'] = 'Фул-хаус '
                    result['combo_index'] = 6

                    put_result()

        def four():
            if self.power > 7 or len(normal_pool) < 5:
                return None

            res = [card for card in normal_pool if pool_vals.count(card.val) == 4]

            if res:
                cards = [card for card in normal_pool if card.val != res[0].val]

                result['cards'] = res + [cards[0]]
                result['text'] = 'Каре '
                result['combo_index'] = 7

                put_result()

        def straight_flush():
            if len(normal_pool) < 5:
                return None

            def check_sf(cards):
                straight_test = True
                flush_test = True
                x = 0
                while x < 4:
                    straight_test = straight_test and cards[x].val - 1 == cards[x+1].val
                    flush_test = flush_test and cards[x].sui == cards[x+1].sui
                    x += 1
                return {'straight_test': straight_test, 'flush_test': flush_test, 'cards': cards}

            res_list = []

            if 14 in pool_vals:
                x = 0
                while True:
                    pre_res = check_sf(ace_pool[-5-x:][:5])
                    res_list.append(pre_res)
                    x += 1
                    if x == len(ace_pool) - 4:
                        break

            x = 0
            while True:
                pre_res = check_sf(normal_pool[-5-x:][:5])
                res_list.append(pre_res)
                x += 1
                if x == len(normal_pool) - 4:
                    break

            for res in res_list:
                if res['straight_test'] and res['flush_test']:

                    result['cards'] = res['cards']
                    result['text'] = 'Стрит-флэш '
                    result['combo_index'] = 8

                    put_result()

        straight_flush()
        four()
        full_house()
        flush()
        straight()
        three()
        two_pairs()
        one_pair()
        high_card()

    def __init__(self, pool):
        super(Combo, self).__init__()
        self.pool = pool
        self.power = 0
        self.text = ''
        self.cards = []
        self.test_cards()

if __name__ == '__main__':
    main()

# Часть 4

import sys


class GameConfig(object):
    """Первоначальные настройки для класса Game"""
    n_of_players = 6
    all_players_bank = 6000
    states = ['pre_flop', 'flop', 'turn', 'river']
    double_blinds = True
    double_blinds_interval = n_of_players
    start_sb = 50
    start_bb = start_sb * 2
    players_names = [
        'Андрей',
        'Василий',
        'Пётр',
        'Сергей',
        'Ольга',
        'Мария',
        'Вадим',
        'Дмитрий',
        'Антон',
    ]

    def __init__(self):
        super(GameConfig, self).__init__()


class PlayerConfig(object):
    """docstring for Player_Config"""
    def __init__(self):
        super(PlayerConfig, self).__init__()


class CardConfig(object):
    """Первоначальные настройки для класса Card"""
    if sys.stdout.encoding == 'cp866':  # Консоль windows
        suits = [chr(3), chr(4), chr(5), chr(6)]
    elif sys.stdout.encoding == 'UTF-8':  # unix
        suits = [chr(0x2665), chr(0x2666), chr(0x2663), chr(0x2660)]
    elif sys.stdout.encoding == 'cp1251':  # GUI windows
        suits = ['h', 'd', 'c', 's']
    vals = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']

    def __init__(self):
        super(CardConfig, self).__init__()


class ComboConfig(object):
    """Первоначальные настройки для класса Combo"""

    def __init__(self):
        super(ComboConfig, self).__init__()

if __name__ == '__main__':
    main()

# Часть 5

from .config_class import GameConfig as cfg
from .card_class import Card
from .player_class import Player
import random


class Game(cfg):
    """Класс игры"""
    def __log(self, *args):
        message = ' '.join([str(arg) for arg in args])
        #print(message)
        self.log.append(message)

    def __create_plrs(self):
        self.plrs = []
        bank = cfg.all_players_bank // cfg.n_of_players
        names = cfg.players_names[:]
        random.shuffle(names)

        for plr in range(cfg.n_of_players):
            name = names.pop()
            plr = Player(bank, name)
            self.plrs.append(plr)

    def __select_dealer(self):
        plr = random.choice(self.plrs)
        plr.dealer = True
        self.current_plr_id = self.plrs.index(plr)

    def __init__(self):
        super(Game, self).__init__()
        self.log = []
        self.river = []
        self.bank = 0
        self.min_bit = 0
        self.sb = 0
        self.bb = 0
        self.hand_number = 1
        self.deck = []

        self.__create_plrs()
        self.__select_dealer()

    def __create_deck(self):
        deck = []
        for sui in range(len(Card.suits)):
            for val in range(len(Card.vals)):
                c = Card(sui, val)
                deck.append(c)
        return deck

    def __give_cards(self):
        random.shuffle(self.deck)
        for plr in self.plrs:
            plr.cards = []
            plr_card_1 = [self.deck.pop()]
            plr_card_2 = [self.deck.pop()]
            plr.cards += plr_card_1 + plr_card_2

    def hand_init(self):
        self.deck = self.__create_deck()
        random.shuffle(self.deck)
        self.__give_cards()

    def __next_act_plr(self):
        self.current_plr_id += 1
        if self.current_plr_id > len(self.plrs) - 1:
                self.current_plr_id = 0

        skip_list = ['Fold', 'All-in']
        while self.plrs[self.current_plr_id].last_act in skip_list:
            self.current_plr_id += 1
            if self.current_plr_id > len(self.plrs) - 1:
                self.current_plr_id = 0

        next_plr = self.plrs[self.current_plr_id]

        return next_plr

    def __daler_id(self):
        dealer, = [plr for plr in self.plrs if plr.dealer]

        ind = self.plrs.index(dealer)

        return ind

    def __clear_acts(self, full=False):
        if full:
            for plr in self.plrs:
                plr.last_act = 'None'
        else:
            for plr in self.plrs:
                if plr.last_act != 'Fold' and plr.last_act != 'All-in':
                    plr.last_act = 'None'

    def __clear_bits(self):
        for plr in self.plrs:
            plr.last_bit = 0

    def hand_result(self):

        def select_winner():
            self.__log('=' * 80)
            if len(self.active_plrs(no_fold=True)) == 1:
                winner, = self.active_plrs(no_fold=True)
                winner.bank += self.bank
                self.__log(winner.name, 'не открывая карты')
            else:
                win_combo_power = max(plr.combo.power for plr in self.plrs)
                winners = [plr for plr in self.plrs if plr.combo.power == win_combo_power]
                if len(winners) == 1:
                    winner, = winners
                    winner.bank += self.bank
                    self.__log(winner.name, winner.combo.text)
                else:
                    for plr in winners:
                        plr.bank += self.bank // len(winners)
                        self.__log(', '.join([plr.name for plr in winners]), winners[0].combo.text)
            self.bank = 0
            self.__log('=' * 80)

        def remove_loosers():
            loosers = []
            for plr in self.plrs:
                if plr.bank == 0:
                    loosers.append(plr)

            for looser in loosers:
                self.plrs.remove(looser)

        def move_dealer():
            old_dealer_id = self.__daler_id()
            old_dealer = self.plrs[old_dealer_id]
            old_dealer.dealer = False

            new_dealer_id = old_dealer_id + 1
            if new_dealer_id == len(self.plrs):
                new_dealer_id = 0

            while self.plrs[new_dealer_id] not in self.with_bank_plrs():
                new_dealer_id += 1
                if new_dealer_id == len(self.plrs):
                    new_dealer_id = 0

            new_dealer = self.plrs[new_dealer_id]
            new_dealer.dealer = True

            self.current_plr_id = self.plrs.index(new_dealer)

        select_winner()
        move_dealer()
        remove_loosers()

        self.river = []
        self.__clear_acts('full')

    def with_bank_plrs(self):
        plrs_list = [plr for plr in self.plrs if plr.bank > 0]
        return plrs_list

    def active_plrs(self, no_fold=False, no_allin=False):
        if no_fold and no_allin:
            skip_list = ['Fold', 'All-in']
        elif no_fold:
            skip_list = ['Fold']
        elif no_allin:
            skip_list = ['All-in']

        plrs = [plr for plr in self.plrs if plr.last_act not in skip_list]
        return plrs

    def __process_the_state(self, state):

        def take_blinds():
            if cfg.double_blinds:  # Вычисление блайндов если умножаются
                double_index = (self.hand_number - 1) // cfg.double_blinds_interval  # Делим без остатка номер раздачи (от 0) на промежуток

                self.sb = cfg.start_sb * (2 ** double_index)  # Умножаем стартовый блайнд на 2 в степени double_index
                self.bb = cfg.start_bb * (2 ** double_index)
            else:
                self.sb = cfg.start_sb
                self.bb = cfg.start_bb

            sb_plr = self.__next_act_plr()
            act = sb_plr.small_blind(self.sb)
            self.bank += act['bit']
            self.__log(self.plrs.index(sb_plr), sb_plr.name, sb_plr.last_act, sb_plr.last_bit)

            bb_plr = self.__next_act_plr()
            act = bb_plr.big_blind(self.bb)
            self.bank += act['bit']
            self.__log(self.plrs.index(bb_plr), bb_plr.name, bb_plr.last_act, bb_plr.last_bit)

            self.min_bit = self.bb

        def take_bits():
            while True:

                bb_flag = False
                if len(self.active_plrs(no_fold=True, no_allin=True)) > 1:
                    plr = self.__next_act_plr()

                    if plr.last_bit == self.bb and plr.last_act == 'BB':
                        bb_flag = True
                    elif plr.last_bit == self.min_bit and plr.last_act == 'Raise':
                        break
                else:
                    plr, = self.active_plrs(no_fold=True, no_allin=True)
                    break

                plr.action(self.plrs, self.river, self.min_bit, self.bb)
                self.bank += plr.last_bit

                self.__log(self.plrs.index(plr), plr.name, plr.last_act, plr.last_bit, plr.combo.text)

                if bb_flag and plr.last_bit == 0 and self.min_bit > 0:
                    break

                if plr.last_bit > self.min_bit:
                    self.min_bit = plr.last_bit

        if state == self.states[0]:
            take_blinds()
        elif state == self.states[1]:
            self.river += [self.deck.pop()]
            self.river += [self.deck.pop()]
            self.river += [self.deck.pop()]
        elif state == self.states[2]:
            self.river += [self.deck.pop()]
        elif state == self.states[3]:
            self.river += [self.deck.pop()]

        self.__log(self.hand_number, state, self.__daler_id(), ' '.join([c.name for c in self.river]), '-' * 30)

        take_bits()

        # Очищаем записи о действиях игроков
        self.__clear_acts()
        self.__clear_bits()
        self.min_bit = 0
        self.current_plr_id = self.__daler_id()

    def process_hand(self):
        for state in cfg.states:

            if len(self.active_plrs(no_fold=True, no_allin=True)) < 2:
                break
            self.__process_the_state(state)


if __name__ == '__main__':
    main()

# Часть 6

from .config_class import PlayerConfig as cfg
from .combo_class import Combo
import random


class Player(cfg):
    """docstring for PLayer"""
    def __init__(self, bank, name):
        super(Player, self).__init__()
        self.dealer = False
        self.last_act = 'None'
        self.last_bit = 0
        self.combo = None

        self.bank = bank
        self.name = name

    def __check_combo(self, river):
        check_cards = self.cards + river
        self.combo = Combo(check_cards)

    def action(self, players, river, min_bit, bb):
        self.__check_combo(river)
        actions = ['Fold', 'Check', 'Call', 'Raise', 'All-in']
        max_bank = max([player.bank for player in players])

        if min_bit > 0 and self.last_bit < min_bit:
            actions.remove('Check')

        if min_bit == 0 or min_bit == self.last_bit:
            actions.remove('Fold')

        if self.bank <= min_bit or min_bit == 0 or self.last_bit == min_bit:
            actions.remove('Call')

        if self.bank < min_bit + bb:
            actions.remove('Raise')

        if self.bank > max_bank:
            actions.remove('All-in')

        min_power = 761856

        act_power = self.combo.power // min_power

        if act_power < 1:
            act = actions[0]
        else:
            act = actions[1]

        #act = random.choice(actions)

        if act == 'Check':
            self.last_act = 'Check'
            self.last_bit = 0
        elif act == 'Fold':
            self.last_act = 'Fold'
            self.last_bit = 0
        elif act == 'Call':
            self.last_act = 'Call'
            self.last_bit = min_bit
            self.bank -= self.last_bit
        elif act == 'Raise':
            self.last_act = 'Raise'
            self.last_bit = min_bit + bb
            self.bank -= self.last_bit
        elif act == 'All-in':
            self.last_act = 'All-in'
            self.last_bit = self.bank
            self.bank = 0

    def small_blind(self, sb):
        if self.bank > sb:
            self.last_act = 'SB'
            self.last_bit = sb
            self.bank -= sb
        else:
            self.last_act = 'All-in'
            self.last_bit = self.bank
            self.bank = 0

        return {'name': self.last_act, 'bit': self.last_bit}

    def big_blind(self, bb):
        if self.bank > bb:
            self.last_act = 'BB'
            self.last_bit = bb
            self.bank -= bb
        else:
            self.last_act = 'All-in'
            self.last_bit = self.bank
            self.bank = 0

        return {'name': self.last_act, 'bit': self.last_bit}

if __name__ == '__main__':
    main()